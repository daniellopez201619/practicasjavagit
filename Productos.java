public class Productos {
    //Atributos de la clase
    private String codigo;
    private String descripcion;
    private String unidadMedida;
    private float precioCompra;
    private float precioVenta;
    private int cantidad;
    
    public Productos(){
    this.codigo="";
    this.descripcion="";
    this.unidadMedida="";
    this.precioCompra=0.0f;
    this.precioVenta=0.0f;
    this.cantidad=0;
    }
    
    //Constructor por argumentos
    public Productos(String codigo, String descripcion, String unidadMedida, float precioCompra, float precioVenta, int cantidad) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.unidadMedida = unidadMedida;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.cantidad = cantidad;
    }
    
    //Copia
    public Productos(Productos otro) {
        this.codigo = otro.codigo;
        this.descripcion = otro.descripcion;
        this.unidadMedida = otro.unidadMedida;
        this.precioCompra = otro.precioCompra;
        this.precioVenta = otro.precioVenta;
        this.cantidad = otro.cantidad;
    }
    
    //Metodos Set y Get
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    //Metodos de comportamiento
    public float calcularPrecioVenta(){
    float calcularPrecioVenta=0.0f;
    calcularPrecioVenta = this.precioVenta*this.cantidad;
    return calcularPrecioVenta;
    }
    
    public float calcularPrecioCompra(){
    float calcularPrecioCompra=0.0f;
    calcularPrecioCompra = this.precioCompra*this.cantidad;
    return calcularPrecioCompra;
    }
    
    public float calcularGanancia(){
    float ganancia=0.0f;
    ganancia = this.calcularPrecioVenta()-this.calcularPrecioCompra();
    return ganancia;
    }
}

